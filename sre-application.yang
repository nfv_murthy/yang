// Contents of "sre-application"
module iana-sre-application {
	namespace "urn:ietf:params:xml:ns:yang:iana-sre-application";
	prefix "sre-a";
	import iana-sre-common { prefix "sre-cmn"; }

	import ietf-yang-types { prefix yang; }

	organization "Mitel";
	contact "support@mitel.com";

	description
		"The module for entities implementing the SRE application.";

	revision 2007-06-09 {
		description "Initial revision.";
		reference
			"YANG Data Model for SRE";
	}

	container system {
		description "Defines configuration for SRE";
		list ims_rmt_server {
			key "name";
			max-elements 2048;
			description "Defines IMS Remote Server configuration";
			leaf name {
				type string;
				description "Weight associated with the primary route";
			}
			leaf server_type {
				type sre-cmn:IMS_SERVER_TYPE;
				default IMS_SERVER_TYPE_BGCF;
				description "IMS Server Type";
			}
			leaf protocol {
				type sre-cmn:IP_SERVICE_TYPE;
				default IP_SERVICE_SIP;
				description "IP service type";
			}
			leaf host {
				type string {
					length "1..255";
					pattern "[a-zA-Z][0-9a-zA-Z_-]*";
				}
				description "Peer host name";
			}
			leaf isTrusted {
				type boolean;
				default false;
				description "Trusted node or Untrusted node";
			}
			leaf isBinarySupp {
				type boolean;
				default false;
				description "Does Peer supports Binary protocol";
			}
			leaf port {
				type uint16 {
					range "0..65535";
				}
				default 0;
				description "Listen Port number";
			}
			leaf versionID {
				type uint16;
				default 0;
				description "record revision";
			}
			leaf groupID {
				type uint16 {
					range "1..100";
				}
				default 1;
				description "valid value is 1~100";
			}
			leaf timeStamp {
				type yang:date-and-time;
				description "Last date and time of when this records was updated";
			}
		}

		list app_service_port {
			key "name";
			max-elements 2048;
			description "Defines Application Service Port configuration";

			leaf name {
				type string;
				description "TBD";
			}

			leaf server_type {
				type sre-cmn:IMS_SERVER_TYPE;
				default IMS_SERVER_TYPE_BGCF;
				description "IMS Server Type";
			}
			leaf app_type {
				type sre-cmn:APPLICATION_TYPE;

				must "app_type != 'APP_TYPE_SIP_ROUTER' or " +
					"app_type != 'APP_TYPE_SIP_ROUTER_OPTIONS' "
					{
						error-message "App Type has to be SIP Router or SIP Router Options";
						description "constraint for this data node";

					}
				default 0;
				description "Identifies the type of application";
			}
			leaf ip_service_type {
				type sre-cmn:IP_SERVICE_TYPE;
				default IP_SERVICE_SIP;
				description "IP service type";
			}
			leaf ip_proto_type {
				type sre-cmn:IP_PROTO_TYPE;
				default IP_PROTO_UDP;
				description "IP protocol type";
			}
			leaf isServer {
				type boolean;
				default true;
				description "Indicates whether SRE acts as a server or a client.";
			}
			leaf external_port {
				type uint16 {
					range "0..65535";
				}
				default 0;
				description "Indicates External Side Port number.External port may be 0 for TCP client service. In such cases RM will dynamically allocate the port number. If external_port is non-zero then IP_PROTO_TYPE and external port should be UNIQUE.";
			}

			leaf internal_port {
				type uint16 {
					range "0..65535";
				}
				default 0;
				description "Indicates Internal Side Port number.";
			}
			leaf port_range {
				type uint16 {
					range "0..65535";
				}
				default 0;
				config false;
				description "Port range";
			}
			leaf fkfqdn_local {
				type string;
				description "Weight associated with the primary route";
			}
			leaf fknodeid {
				type uint16 {
					range "0..65535";
				}
				default 0;
				description "Always set to 0";
			}
			leaf versionID {
				type uint16;
				default 0;
				description "record revision";
			}
			leaf timeStamp {
				type yang:date-and-time;
				description "Last date and time of when this records was updated";
			}


		}
		container sre {
			description "Defines SRE application configuration";

			leaf index {
				type uint32;
				mandatory true;
				description "Index value";
			}
			leaf convert_teluri_to_sipuri	{
				type boolean;
				default false;
				description "if yes, tel URI shall be converted to SIP URI";
			}
			leaf max_fowards_value	{
				type uint16 {
					range "1..70";
				}
				default 70;
				description "max forward header value to be used";
			}
			leaf perform_loop_detection	{
				type boolean;
				default false;
				description "if yes, loop detection could be performed";
			}
			leaf enum_gaurd_timer	{
				type uint32;
				units seconds;
				default 10;
				description "application gaurd timer value for ENUM query";
			}
			leaf ss7_gaurd_timer	{
				type uint32;
				units seconds;
				default 10;
				description "application gaurd timer value for HLR query";
			}
			leaf sip_options_interval {
				type uint32 {
					range "0..120";
				}
				default 10;
				description "interval which OPTIONS messages are sent by mOne to the peers" ;
			}
			leaf sip_options_retry {
				type uint32 {
					range "1..10";
				}
				default 3;
				description "number of times mOne shall retry before raising an alarm about
					peer not being available";
			}
		}
		list mproxy_rmt_server {
			key "fk_ims_rmt_server";
			max-elements 2048;
			description "Defines mapping...";
			leaf fk_ims_rmt_server {
				type leafref {
					path "../../ims_rmt_server/name";
				}
				description "Index to ims_rmt_server";
			}
			leaf is_catch_all_node	{
				type boolean;
				default false;
				description "if yes, this entry is catch all node";
			}
			leaf is_option_enabled	{
				type boolean;
				default false;
				description "if yes, SIP OPTIONS sent for this node";
			}
			leaf timeStamp {
				type yang:date-and-time;
				description "Last time the records was updated";
			}
		}
		list trr_rtref {
			key "routeIndex";
			max-elements 2048;
			description "Defines TRR Route reference parameters";
			leaf routeIndex {
				type uint32;
				description "Route reference index";
			}
			leaf fkPrimaryRoute {
				type leafref {
					path "../../ims_rmt_server/name";
				}
				description "Index to ims_rmt_server";
			}
			leaf fkSecondaryRoute {
				type leafref {
					path "../../ims_rmt_server/name";
				}
				description "Index to ims_rmt_server";
			}
			leaf fkTertiaryRoute {
				type leafref {
					path "../../ims_rmt_server/name";
				}
				description "Index to ims_rmt_server";
			}
			leaf description	{
				type string {
					length "0..50";
				}
				description "Description for this entry";
			}
			leaf timeStamp {
				type yang:date-and-time;
				description "Last time the records was updated";
			}
		}
		list primary_routes {
			key "oid_index";
			max-elements 1000;
			description "Defines primary routes";
			leaf oid_index {
				type uint32;
				description "Primary Index value";
			}
			leaf weight {
				type uint32;
				description "Weight associated with the primary route";
			}
			leaf fkRouteIndex {
				type leafref {
					path "../../trr_rtref/routeIndex";
				}
				description "Index to trr_rtref";
			}
			leaf fkPrimaryRoute {
				type leafref {
					path "../../ims_rmt_server/name";
				}
				description "Index to ims_rmt_server";
			}
			leaf timeStamp {
				type yang:date-and-time;
				description "Last time the records was updated";
			}
		}
		list trr_fnpachild {
			key "fnpaPrefix cldpnLen";
			max-elements 65535;
			description "Defines FNPA Child configuration,The FNPA Child routing type is used for NPAXX-based routing";
			leaf fnpaPrefix	{
				type string {
					length "0..16";
					pattern "[a-zA-Z][0-9a-zA-Z_-]*";
					/*Need to check size 16 length*/ }
				description "Prefix of foreign network npanxx range";
			}
			leaf cldpnLen {
				/*Need to check RINTEGER type*/
				type uint32 {
					range "0..16";
				}
				description "Called party number Length. '0' for Any length wildcard.";
			}
			leaf fkRouteIndex {
				type leafref {
					path "../../trr_rtref/routeIndex";
				}
				description "Index to trr_rtref";
			}
			leaf causecode	{
				type sre-cmn:TRR_FC_CAUSECODE;
				mandatory true;
				description "FNPA child routing cause code";
			}
			leaf description	{
				type string {
					length "0..50";
				}
				description "A short Description for this entry";
			}
			leaf timeStamp {
				type yang:date-and-time;
				description "Last time the records was updated";
			}
		}
		list trr_fnpahead {
			key "fnpaPrefix cldpnLen";
			max-elements 65535;
			description "Defines FNPA head configuration, The TRR FNPA routing is used for Foreign NPAXX based routing.";
			leaf fnpaPrefix	{
				type string {
					length "0..16";
					pattern "[a-zA-Z][0-9a-zA-Z_-]*";
				}
				description "Prefix of foreign network npanxx range";
			}
			leaf cldpnLen {
				/*RINTEGER*/ type uint32 {
					range "0..16";
				}
				description "Called party number Length. '0' for Any length wildcard.";
			}
			leaf fkRouteIndex {
				type leafref {
					path "../../trr_rtref/routeIndex";
				}
				description "Index to trr_rtref";
			}
			leaf causecode	{
				type sre-cmn:TRR_FH_CAUSECODE;
				mandatory true;
				description "FNPA Head routing cause code";
			}
			leaf description	{
				type string {
					length "0..50";
				}
				description "A short Description for this entry";
			}
			leaf timeStamp {
				type yang:date-and-time;
				description "Last time the records was updated";
			}
		}
		list trr_hnpachild {
			key "hnpaPrefix cldpnLen";
			max-elements 65535;
			description "Defines HNPA child configuration,TRR HNPA routing is used for Home NPAXX based routing";
			leaf hnpaPrefix	{
				type string {
					length "0..16";
					pattern "[a-zA-Z][0-9a-zA-Z_-]*";
				}
				description "Prefix of home network npanxx range";
			}
			leaf cldpnLen {
				/*RINTEGER */
				type uint32 {
					range "0..16";
				}
				description "Called party number Length. '0' for Any length wildcard.";
			}

			leaf fkRouteIndex {
				type leafref {
					path "../../trr_rtref/routeIndex";
				}
				description "Index to trr_rtref";
			}
			leaf causecode	{
				type sre-cmn:TRR_HC_CAUSECODE;
				mandatory true;
				description "HNPA Child routing cause code";
			}
			leaf description	{
				type string {
					length "0..50";
				}
				description "A short Description for this entry";
			}
			leaf timeStamp {
				type yang:date-and-time;
				description "Last time the records was updated";
			}
		}
		list trr_hnpahead {
			key "hnpaPrefix cldpnLen";
			max-elements 65535;
			description "Defines HNPA head configuration,TRR HNPA routing is used for Home NPAXX based routing";
			leaf hnpaPrefix	{
				type string {
					length "0..16";
					pattern "[a-zA-Z][0-9a-zA-Z_-]*";
				}
				description "Prefix of home network npanxx range";
			}
			leaf cldpnLen {
				/*RINTEGER */
				type uint32 {
					range "0..16";
				}
				description "Called party number Length. '0' for Any length wildcard.";
			}

			leaf fkRouteIndex {
				type leafref {
					path "../../trr_rtref/routeIndex";
				}
				description "Index to trr_rtref";
			}
			leaf causecode	{
				type sre-cmn:TRR_HH_CAUSECODE;
				mandatory true;
				description "HNPA Head routing cause code";
			}
			leaf description	{
				type string {
					length "0..50";
				}
				description "A short Description for this entry";
			}
			leaf timeStamp {
				type yang:date-and-time;
				description "Last time the records was updated";
			}
		}
		list trr_nwrte {
			key "nwknodePrefix nwknodeAddrLen";
			max-elements 4000;
			description "TRR network node number routing is used for Network Node Number based routing";

			leaf nwknodePrefix	{
				type string {
					length "0..16";
					pattern "[a-zA-Z][0-9a-zA-Z_-]*";
				}
				description "Prefix of network node number";
			}
			leaf nwknodeAddrLen {
				/*RINTEGER */
				type uint32 {
					range "0..16";
				}
				description "Called party number Length. '0' for Any length wildcard.";
			}

			leaf fkRouteIndex {
				type leafref {
					path "../../trr_rtref/routeIndex";
				}
				description "Index to trr_rtref";
			}
			leaf causecode	{
				type sre-cmn:TRR_NR_CAUSECODE;
				mandatory true;
				description "Network node number based routing cause code";
			}
			leaf description	{
				type string {
					length "0..50";
				}
				description "A short Description for this entry";
			}
			leaf timeStamp {
				type yang:date-and-time;
				description "Last time the records was updated";
			}
		}
		list trr_nwrtscrn {
			key "cldPnPrefix cldPnAddrLen"; /*{ CONSTRAINTUNIQUE { cldPnPrefix cldPnAddrLen }}*/
			max-elements 5000;
			description "Defines Network Route Screen configuration";

			leaf cldPnPrefix {
				type string {
					length "0..16";
					pattern "[a-zA-Z][0-9a-zA-Z_-]*";
				}
				description "Called Party number destination address";
			}
			leaf cldPnAddrLen {
				/*RINTEGER */
				type uint32 {
					range "0..16";
				}
				description "Network Node address Length. '0 'for Any length wildcard.";
			}

			leaf fkRouteIndex {
				type leafref {
					path "../../trr_rtref/routeIndex";
				}
				description "Index to trr_rtref";
			}
			leaf causecode	{
				type sre-cmn:TRR_NR_CAUSECODE;
				mandatory true;
				description "Network node number based routing cause code";
			}
			leaf description	{
				type string {
					length "0..50";
				}
				description "A short Description for this entry";
			}
			leaf timeStamp {
				type yang:date-and-time;
				description "Last time the records was updated";
			}
		}
		list trr_spiddomain {
			key "spid spidLen"; /*{ CONSTRAINTUNIQUE { spid spidLen }}*/
			max-elements 5000;
			description "Defines SPID Domain parameters";
			leaf spid {
				type string {
					length "0..16";
					pattern "[a-zA-Z][0-9a-zA-Z_-]*";
				}
				description "Service provider ID";
			}
			leaf spidLen {
				/*RINTEGER */
				type uint32 {
					range "0..16";
				}
				description "SPID Length. '0' for Any length wildcard";
			}

			leaf fkRouteIndex {
				type leafref {
					path "../../trr_rtref/routeIndex";
				}
				description "Index to trr_rtref";
			}
			leaf network	{
				type sre-cmn:TRR_SD_NETWORK;
				mandatory true;
				description "Indicating Home or Partner network";
			}
			leaf causecode	{
				type sre-cmn:TRR_SD_CAUSECODE;
				mandatory true;
				description "SPID Domain routing cause code";
			}
			leaf description	{
				type string {
					length "0..50";
				}
				description "A short Description for this entry";
			}
			leaf timeStamp {
				type yang:date-and-time;
				description "Last time the records was updated";
			}
		}
		list trr_unvxla {
			key "fkRouteIndex unvPrefix";
			unique "unvPrefix cldpnLen"; /*{ CONSTRAINTUNIQUE { unvPrefix cldpnLen }}*/
			max-elements 65535;
			description "The TRR Universal NPAXX routing is used for Universal NPAXX based routing. Universal NPA are for all other prefixes other than HOME & PARTNER prefixes.";

			leaf unvPrefix {
				type string {
					length "0..16";
					pattern "[a-zA-Z][0-9a-zA-Z_-]*";
				}
				description "Prefix of other network npanxx range.";
			}
			leaf cldpnLen {
				/*RINTEGER */
				type uint32 {
					range "0..16";
				}
				mandatory true;
				description "Called party number Length. '0' for Any length wildcard.";
			}

			leaf fkRouteIndex {
				type leafref {
					path "../../trr_rtref/routeIndex";
				}
				description "Route Index to trr_rtref";
			}
			leaf causecode	{
				type sre-cmn:TRR_UX_CAUSECODE;
				mandatory true;
				description "Universal NPAXX routing cause code";
			}
			leaf description	{
				type string {
					length "0..50";
				}
				description "A short Description for this entry";
			}
			leaf timeStamp {
				type yang:date-and-time;
				description "Last time the record was updated";
			}
		}

	}
}
